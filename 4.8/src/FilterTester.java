
/**
 * Finds strings of three characters or less in a string array
 *
 * @author Matthew Hwang cs151 Homework 2
 */
public class FilterTester
{

   /**
    * Uses custom interface to find strings with 3 or less characters
    *
    * @param a array of strings
    * @param f custom compare method
    * @return array of strings
    */
   public static String[] filter(String[] a, Filter f)
   {
      String[] temp = new String[a.length];
      int count = 0;
      for (int i = 0; i < a.length; i++)
      {
         if (f.accept(a[i]) == true)
         {
            temp[count] = a[i];
            count++;
         }
      }
      String[] three = new String[count];
      for (int i = 0; i < count; i++)
      {
         three[i] = temp[i];
      }
      return three;
   }

   /**
    * Uses a local inner class to compare string lengths
    *
    * @param x a string
    * @return boolean, true is less than or equal to 3 characters, false if not
    */
   public static class ThreeChar implements Filter
   {

      @Override
      public boolean accept(String x)
      {
         if (x.length() <= 3)
         {
            return true;
         }
         else
         {
            return false;
         }
      }
   }

   public static void main(String[] args)
   {
      String[] a =
      {
         "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten"
      };
      System.out.println("Should return one, two, six, and ten");
      ThreeChar n = new ThreeChar();

      String[] b = filter(a, n);
      for (int i = 0; i < b.length; i++)
      {
         System.out.println(b[i]);
      }
   }
}
