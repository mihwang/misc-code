
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

/**
 * Creates and sorts an array of rectangles in lexicographical order, meaning if
 * we are given set1(x1,y1), and set2(x2,y2) then set1 <= set2 if and only if x1
 * < x2 or x1=x2 && y1 < y2.
 *
 * @author Matthew Hwang cs 151 homework 2
 */
public class RectangleTester
{

   /**
    * Sorts an arraylist of rectangles
    *
    * @param a arraylist of rectangles
    * @return sorted arraylist
    */
   public static ArrayList<Rectangle2D.Double> sort(ArrayList<Rectangle2D.Double> a)
   {
      ArrayList<Rectangle2D.Double> r = a;


      for (int i = 0; i < a.size(); i++)
      {
         Rectangle2D.Double lowest = r.get(i);
         int index = i;
         for (int j = i; j < a.size(); j++)
         {
            RectangleComparator rec = new RectangleComparator(lowest);
            if (rec.compareTo(r.get(j)) == 1)
            {
               lowest = r.get(j);
               index = j;
            }
         }
         //swaps
         Rectangle2D.Double temp = a.get(i);
         r.set(i, lowest);
         r.set(index, temp);
      }
      return r;
   }

   public static void main(String[] args)
   {
      ArrayList<Rectangle2D.Double> list = new ArrayList<>();
      Rectangle2D.Double d1 = new Rectangle2D.Double(50, 50, 90, 90);
      Rectangle2D.Double d2 = new Rectangle2D.Double(10, 50, 90, 90);
      Rectangle2D.Double d3 = new Rectangle2D.Double(50, 25, 90, 90);
      Rectangle2D.Double d4 = new Rectangle2D.Double(50, 50, 75, 90);
      list.add(d1);
      list.add(d2);
      list.add(d3);
      list.add(d4);
      System.out.println("The order should be :");
      System.out.println(d2 + "\n" + d3 + "\n" + d4 + "\n" + d1);
      System.out.println("The resulted order is:");
      ArrayList<Rectangle2D.Double> newList = sort(list);
      for (int i = 0; i < newList.size(); i++)
      {
         System.out.println(newList.get(i));
      }

   }
}