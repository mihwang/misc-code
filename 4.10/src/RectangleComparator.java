
import java.awt.geom.Rectangle2D;

/**
 * Compares elements that make up a rectangle
 *
 * @author Matthew
 */
public class RectangleComparator implements Comparable
{

   private double x;//x co-ordinate
   private double y;//y co-ordinate
   private double width;
   private double height;

   /**
    * initial constructor
    *
    * @param d 2D rectangle
    */
   public RectangleComparator(Rectangle2D.Double d)
   {
      x = d.x;
      y = d.y;
      width = d.width;
      height = d.height;
   }

   /**
    * Compares rectangles lexicographically
    *
    * @param o rectangle object
    * @return -1 if smaller, 0 if same, 1 if bigger
    */
   @Override
   public int compareTo(Object o)
   {

      Rectangle2D.Double d = (Rectangle2D.Double) o;
      if (x != d.x)
      {
         if (x < d.x)
         {
            return -1;
         }
         else
         {
            return 1;
         }
      }
      else if (y != d.y)
      {
         if (y < d.y)
         {
            return -1;
         }
         else
         {
            return 1;
         }
      }
      else if (width != d.width)
      {
         if (width < d.width)
         {
            return -1;
         }
         else
         {
            return 1;
         }
      }
      else if (height != d.height)
      {
         if (height < d.height)
         {
            return -1;
         }
         else
         {
            return 1;
         }
      }
      return 0;


   }
}