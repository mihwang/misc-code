
import javax.swing.*;

/**
 * Gpit class. Adds stones to the pits
 *
 * @author: Amru Eliwat, Matthew Hwang, Chau Ngo
 * @class: CS 151
 * @section 1
 * @Team: Area 151
 * @date: May 4th, 2013
 */
public class Gpit extends JComponent
{

   private String player; // player number
   private boolean isClicked = false; // if clicked
   private int column; // column number
   private PitIcon stoneIcon;

   /**
    * Constructs pits for player
    *
    * @param playername player number
    * @param icon stone icons
    */
   public Gpit(String playername, Icon icon)
   {

      player = playername; // player number
      stoneIcon = (PitIcon) icon;

      JButton button = new JButton();
      button.setIcon(icon);
      setVisible(true);
   }

   /**
    * Getter for player.
    *
    * @return selected player
    */
   public String getPlayer()
   {
      return player;
   }

   /**
    * Determines if Gpit was clicked
    *
    * @return true is yes, false if no
    */
   public boolean isClicked()
   {
      return isClicked;
   }

   /**
    * getter for stoneIcon
    *
    * @return selected stoneIcon
    */
   public PitIcon getStoneIcon()
   {
      return stoneIcon;
   }

   /**
    * Getter for column number
    *
    * @return selected column
    */
   public int getColumn()
   {
      return this.column;
   }

   /**
    * Sets column
    *
    * @param col column number
    */
   public void setColumn(int col)
   {
      this.column = col;
   }

   /**
    * Sets player
    *
    * @param player selected player
    */
   public void setPlayer(String player)
   {
      this.player = player;
   }
}
