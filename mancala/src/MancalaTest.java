
import javax.swing.*;

/**
 * Program that recreates the board game Mancala.
 *
 * @author: Amru Eliwat, Matthew Hwang, Chau Ngo
 * @class: CS 151
 * @section 1
 * @Team: Area 151
 * @date: May 4th, 2013
 */
public class MancalaTest extends JFrame
{

   public static void main(String[] args)
   {
      // Create and set up the window.
      JFrame frame = new JFrame("Options Menu");
      frame.setSize(600, 500);
      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

      // Create and set up the content pane.
      JComponent newContentPane = new Menu();
      newContentPane.setOpaque(true); // content panes must be opaque
      frame.setContentPane(newContentPane);

      // Display the window.
      frame.setVisible(true);
   }
}
