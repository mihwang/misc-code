
/**
 * Pit class. Creates pits
 *
 * @author: Amru Eliwat, Matthew Hwang, Chau Ngo
 * @class: CS 151
 * @section 1
 * @Team: Area 151
 * @date: May 4th, 2013
 */
public class Pit
{

   private int stones;//stones in the pit

   /**
    * Constructs pit with some number of stones
    *
    * @param number number of stones
    */
   Pit(int number)
   {
      this.stones = number;
   }

   /**
    * sets stones into a pit
    *
    * @param stones number of stones
    */
   public void setStones(int stones)
   {
      this.stones = stones;
   }

   /**
    * gets number of stones
    *
    * @return number of stones
    */
   public int getStones()
   {
      return stones;
   }

   /**
    * Adds one stone to a pit
    */
   public void addStone()
   {
      this.stones++;
   }

   /**
    * Takes stone(s) from a pit
    */
   public void subtractStone()
   {
      this.stones--;
   }

   /**
    * Generic toString method
    *
    * @return number of stones as a string
    */
   @Override
   public String toString()
   {
      return Integer.toString(stones);
   }
}
