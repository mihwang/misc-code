
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.awt.geom.RoundRectangle2D;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.Icon;

/**
 * PitIcon class. Draws pits
 *
 * @author: Amru Eliwat, Matthew Hwang, Chau Ngo
 * @class: CS 151
 * @section 1
 * @Team: Area 151
 * @date: May 4th, 2013
 */
public class PitIcon implements Icon
{

   int x = 20;// x-coordinate
   int y = 40;// y-coordinate
   String side;//side pits
   String number;
   Color stoneColor;
   String numberOfStones;
   Random rand = new Random();
   Ellipse2D.Double stone;
   RoundRectangle2D.Double roundBorder;
   final double STONE_WIDTH = 15;
   final double STONE_HEIGHT = 15;
   ArrayList<Shape> stoneList = new ArrayList<Shape>();

   /**
    * Constructs icon for pit
    *
    * @param qty number of stones
    * @param sColor color of stones
    * @param aSide selected side
    * @param aNumber
    */
   public PitIcon(int qty, Color sColor, int aSide, int aNumber)
   {
      stoneColor = sColor;
      number = Integer.toString(aNumber + 1);
      if (aSide == 0)
      {
         side = "B";
      }
      else
      {
         side = "A";
      }

      numberOfStones = Integer.toString(qty);
      roundBorder = new RoundRectangle2D.Double(10, 10, 80, 120, 20, 20);

      for (int i = 0; i < qty; i++)
      {
         stone = new Ellipse2D.Double(x, y, STONE_WIDTH, STONE_HEIGHT);
         stone = new Ellipse2D.Double(x, y, STONE_WIDTH, STONE_HEIGHT);
         stoneList.add(stone);
         if (x < 60)
         {
            x += 15;
         }
         else
         {
            x = 20;
            y += 15;
         }
      }
   }

   @Override
   public int getIconHeight()
   {
      return (int) STONE_HEIGHT;
   }

   @Override
   public int getIconWidth()
   {
      return (int) STONE_WIDTH;
   }

   @Override
   public void paintIcon(Component c, Graphics g, int x, int y)
   {
      Graphics2D g2 = (Graphics2D) g;

      for (Shape s : stoneList)
      {
         g2.setPaint(stoneColor);
         g2.fill(s);
      }
      g2.setColor(Color.BLACK);
      for (Shape s : stoneList)
      {
         g2.draw(s);
      }

      // draw border
      BasicStroke stroke = new BasicStroke(3);
      g2.setStroke(stroke);
      g2.draw(roundBorder);

      // draw number of stones currently in pit
      g2.drawString(numberOfStones, 20, 125);

      // draw pit names
      g2.drawString(side, 42, 28);
      g2.drawString(number, 50, 28);
   }
}
