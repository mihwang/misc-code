
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * Menu class. Creates menu which has various options
 *
 * @author: Amru Eliwat, Matthew Hwang, Chau Ngo
 * @class: CS 151
 * @section 1
 * @Team: Area 151
 * @date: May 4th, 2013
 */
public class Menu extends JPanel
{
   // check boxes for each option

   JCheckBox threestones;
   JCheckBox fourstones;
   JCheckBox redstones;
   JCheckBox yellowstones;
   JCheckBox whitestones;
   JCheckBox grayboard;
   JCheckBox greenboard;
   JCheckBox blueboard;
   // label for logo
   JLabel logoLabel;
   // start button
   JButton playgame;
   static Color boardcolor = new Color(225, 225, 225);
   static String startstones = "3";//default option
   static Color stoneColor = Color.RED;

   /**
    * Constructs menu
    */
   public Menu()
   {
      super(new BorderLayout());

      // create label
      JLabel numberStones = new JLabel("Choose number of staring stones:");
      JLabel colorStones = new JLabel("Choose color of stones: ");
      JLabel colorBoard = new JLabel("Choose color of Mancala board: ");

      // create checkboxes
      threestones = new JCheckBox("3 Stones (default)");
      fourstones = new JCheckBox("4 Stones");
      redstones = new JCheckBox("Red");
      yellowstones = new JCheckBox("Yellow");
      whitestones = new JCheckBox("White");
      grayboard = new JCheckBox("Gray");
      greenboard = new JCheckBox("Green");
      blueboard = new JCheckBox("Blue");

      // Set up the logo label
      logoLabel = new JLabel();
      ImageIcon logo = new ImageIcon("src/alien_area151.png");
      logoLabel.setIcon(logo);

      // Set up start button
      playgame = new JButton("Play Game");

      // Put the check boxes and start button in a column in a panel
      JPanel checkPanel = new JPanel(new GridLayout(0, 1));
      checkPanel.add(numberStones);
      checkPanel.add(threestones);
      checkPanel.add(fourstones);
      checkPanel.add(colorStones);
      checkPanel.add(redstones);
      checkPanel.add(yellowstones);
      checkPanel.add(whitestones);
      checkPanel.add(colorBoard);
      checkPanel.add(grayboard);
      checkPanel.add(greenboard);
      checkPanel.add(blueboard);
      checkPanel.add(playgame);

      // Position checkPanel and LogoLabel
      add(checkPanel, BorderLayout.CENTER);
      add(logoLabel, BorderLayout.LINE_START);
      setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));

      threestones.addActionListener(new ActionListener()
      {
         @Override
         public void actionPerformed(ActionEvent arg0)
         {
            startstones = "3";
         }
      });

      fourstones.addActionListener(new ActionListener()
      {
         @Override
         public void actionPerformed(ActionEvent arg0)
         {
            startstones = "4";
         }
      });

      redstones.addActionListener(new ActionListener()
      {
         @Override
         public void actionPerformed(ActionEvent arg0)
         {
            stoneColor = Color.RED;
         }
      });

      yellowstones.addActionListener(new ActionListener()
      {
         @Override
         public void actionPerformed(ActionEvent arg0)
         {
            stoneColor = Color.YELLOW;
         }
      });

      whitestones.addActionListener(new ActionListener()
      {
         @Override
         public void actionPerformed(ActionEvent arg0)
         {
            stoneColor = Color.WHITE;
         }
      });

      grayboard.addActionListener(new ActionListener()
      {
         @Override
         public void actionPerformed(ActionEvent arg0)
         {
            boardcolor = new Color(225, 225, 225);
         }
      });

      blueboard.addActionListener(new ActionListener()
      {
         @Override
         public void actionPerformed(ActionEvent arg0)
         {
            boardcolor = new Color(16, 111, 236);
         }
      });

      greenboard.addActionListener(new ActionListener()
      {
         @Override
         public void actionPerformed(ActionEvent arg0)
         {
            boardcolor = new Color(0, 199, 140);
         }
      });

      playgame.addActionListener(new ActionListener()
      {
         @Override
         public void actionPerformed(ActionEvent e)
         {

            View view = new View(boardcolor);
            view.startGame(startstones, stoneColor);
         }
      });
   }
}