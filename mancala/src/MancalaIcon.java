
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.awt.geom.RoundRectangle2D;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.Icon;

/**
 * MancalaIcon class. Draws pits ands tones
 *
 * @author: Amru Eliwat, Matthew Hwang, Chau Ngo
 * @class: CS 151
 * @section 1
 * @Team: Area 151
 * @date: May 4th, 2013
 */
public class MancalaIcon implements Icon
{

   int x = 20;//x-coordinate
   int y = 80;// y-coordinate
   Color stoneColor;
   String name;
   String numberOfStones;
   Random rand = new Random();
   Ellipse2D.Double stone;
   RoundRectangle2D.Double roundBorder;
   final double STONE_WIDTH = 15;
   final double STONE_HEIGHT = 15;
   ArrayList<Shape> stoneList = new ArrayList<Shape>();

   /**
    * Constructs icon with stone quantity, stone color and stone count
    *
    * @param qty stone number
    * @param sColor stone color
    * @param aName stone count
    */
   public MancalaIcon(int qty, Color sColor, String aName)
   {

      name = aName;
      stoneColor = sColor;

      numberOfStones = Integer.toString(qty);
      roundBorder = new RoundRectangle2D.Double(10, 10, 80, 260, 20, 20);

      for (int i = 0; i < qty; i++)
      {
         stone = new Ellipse2D.Double(x, y, STONE_WIDTH, STONE_HEIGHT);
         stone = new Ellipse2D.Double(x, y, STONE_WIDTH, STONE_HEIGHT);
         stoneList.add(stone);
         if (x < 60)
         {
            x += 15;
         }
         else
         {
            x = 20;
            y += 15;
         }
      }
   }

   /**
    * gets height. Unnecessary method
    *
    * @return nothing
    */
   @Override
   public int getIconHeight()
   {
      return 0;
   }

   /**
    * gets width. Unnecessary method
    *
    * @return nothing
    */
   @Override
   public int getIconWidth()
   {
      return 0;
   }

   /**
    * Paints icon
    *
    * @param c component
    * @param g graphics
    * @param x x-coordinate
    * @param y y-coordinate
    */
   @Override
   public void paintIcon(Component c, Graphics g, int x, int y)
   {
      Graphics2D g2 = (Graphics2D) g;

      for (Shape s : stoneList)
      {
         g2.setPaint(stoneColor);
         g2.fill(s);
      }
      g2.setColor(Color.BLACK);
      for (Shape s : stoneList)
      {
         g2.draw(s);
      }

      // draw border
      BasicStroke stroke = new BasicStroke(3);
      g2.setStroke(stroke);
      g2.draw(roundBorder);

      // draw number of stones currently in mancala
      g2.drawString(numberOfStones, 20, 260);

      // draw pit names
      g2.drawString(name, 22, 30);


   }
}
