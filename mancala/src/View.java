
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * View class. Creates the GUI frames along with buttons and listeners.
 *
 * @author: Amru Eliwat, Matthew Hwang, Chau Ngo
 * @class: CS 151
 * @section 1
 * @Team: Area 151
 * @date: May 4th, 2013
 */
public class View extends JFrame implements ChangeListener
{

   private Color color;//board color
   private int startstones = 3;//default stones
   private Model model;
   private Color stoneColor;
   //side pit labels
   private String mancalaA = "Mancala A";
   private String mancalaB = "Mancala B";
   final JLabel message = new JLabel("Either Players Turn");
   //final JLabel score = new JLabel("First Player     :    ");
   //final JLabel score1 = new JLabel("Second Player   :    ");
   final JFrame frame = new JFrame("Mancala game");
   final JPanel mainPanel = new JPanel();
   final JPanel stonePanel = new JPanel();
   final JButton undo = new JButton("UNDO");
   // Panels for each side of the mainPanel
   final JPanel leftPanel = new JPanel();
   final JPanel rightPanel = new JPanel();
   final JPanel topPanel = new JPanel();
   final JPanel downPanel = new JPanel();
   final JPanel centerPanel = new JPanel();
   //final JPanel centerInner = new JPanel(new FlowLayout());
   final JPanel messagePanel = new JPanel();

   /**
    * Constructs main board.
    *
    * @param color board color
    */
   public View(Color color)
   {
      this.color = color;
      undo.setEnabled(false);

      // decorate
      leftPanel.setPreferredSize(new Dimension(150, frame.getHeight() - 200));
      rightPanel
              .setPreferredSize(new Dimension(150, frame.getHeight() - 200));
      topPanel.setPreferredSize(new Dimension(frame.getWidth(), 100));
      messagePanel.setPreferredSize(new Dimension(900, 50));

      leftPanel.setBackground(this.color);
      rightPanel.setBackground(this.color);
      topPanel.setBackground(Color.white);
      downPanel.setBackground(Color.white);
      centerPanel.setBackground(this.color);
      messagePanel.setBackground(Color.white);

      mainPanel.setLayout(new BorderLayout());

      // Add to top panel
      messagePanel.add(message);
      topPanel.add(messagePanel, BorderLayout.CENTER);


      // mainPanel contains all the panels
      mainPanel.add(centerPanel, BorderLayout.CENTER);
      mainPanel.add(topPanel, BorderLayout.NORTH);
      mainPanel.add(downPanel, BorderLayout.SOUTH);
      mainPanel.add(leftPanel, BorderLayout.WEST);
      mainPanel.add(rightPanel, BorderLayout.EAST);
      downPanel.add(undo, BorderLayout.WEST);

      mainPanel.setVisible(true);
      frame.add(stonePanel, BorderLayout.CENTER);
      frame.setSize(900, 500);
      frame.setVisible(true);
      frame.add(mainPanel);
      frame.setContentPane(mainPanel);
      frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);

      //undo listener
      undo.addActionListener(new ActionListener()
      {
         @Override
         public void actionPerformed(ActionEvent event)
         {
            model.input(2, 0);

            Component manc1 = rightPanel.getComponent(0);

            final MancalaIcon imgMac1 = new MancalaIcon(model.board[0][6]
                    .getStones(), stoneColor, mancalaA);
            ((JButton) manc1).setIcon(imgMac1);

            Component manc2 = leftPanel.getComponent(0);
            final MancalaIcon imgMac2 = new MancalaIcon(model.board[1][6]
                    .getStones(), stoneColor, mancalaB);
            ((JButton) manc2).setIcon(imgMac2);

            message.setText("");

            Component[] buttons = centerPanel.getComponents();
            int x = 0;

            for (int i = 1; i >= 0; i--)
            {
               for (int j = 0; j < 6; j++)
               {
                  final Gpit pit;
                  if (i == 0)
                  {
                     pit = new Gpit("", new PitIcon(
                             model.board[i][j].getStones(), stoneColor, i - 1, j));
                  }
                  else
                  {
                     pit = new Gpit("", new PitIcon(
                             model.board[i][5 - j].getStones(), stoneColor, i - 1, 5 - j));
                  }

                  if (i == 0)
                  {
                     pit.setColumn(j);
                  }
                  else
                  {
                     pit.setColumn(5 - j);
                  }

                  if (i == 1)
                  {
                     pit.setPlayer("Player1");
                  }
                  else
                  {
                     pit.setPlayer("Player2");
                  }

                  final PitIcon img = pit.getStoneIcon();
                  ((JButton) buttons[x]).setIcon(img);
                  x++;
               }
            }
            frame.validate();
            frame.repaint();
         }
      });
   }

   /**
    * Starts the game, going from the menu GUi to the board GUI.
    *
    * @param stones number of starting stones
    * @param sColor stone color
    */
   public void startGame(String stones, Color sColor)
   {
      stoneColor = sColor;

      //creates initial starting stones
      if (stones.trim().equals("4"))
      {
         startstones = 4;
         model = new Model(4);
      }
      else
      {
         startstones = 3;
         model = new Model(3);
      }

      final JButton player1_manc = new JButton();
      final JButton player2_manc = new JButton();
      player2_manc.setIcon(new MancalaIcon(0, stoneColor, mancalaB));
      player2_manc.setBackground(color);
      player2_manc.setBorderPainted(false);

      player1_manc.setIcon(new MancalaIcon(0, stoneColor, mancalaA));
      player1_manc.setBackground(color);
      player1_manc.setBorderPainted(false);

      player1_manc.setPreferredSize(new Dimension(100, 300));
      player2_manc.setPreferredSize(new Dimension(100, 300));

      rightPanel.add(player1_manc);
      leftPanel.add(player2_manc);

      GridLayout mancala = new GridLayout(0, 6);
      centerPanel.removeAll();
      centerPanel.setLayout(mancala);

      // start making buttons
      // add Gpits to centerpanel with actionlistener
      for (int i = 0; i < 2; i++)
      {
         for (int j = 0; j < 6; j++)
         {
            final Gpit pit;
            if (startstones == 3)
            {
               if (i == 0)
               {
                  pit = new Gpit("", new PitIcon(3, stoneColor, i, 5 - i));
               }
               else
               {
                  pit = new Gpit("", new PitIcon(3, stoneColor, i, j));
               }
            }
            else if (startstones == 4)
            {
               if (i == 0)
               {
                  pit = new Gpit("", new PitIcon(4, stoneColor, i, 5 - j));
               }
               else
               {
                  pit = new Gpit("", new PitIcon(4, stoneColor, i, j));
               }
            }
            else if (i == 0)
            {
               pit = new Gpit("", new PitIcon(3, stoneColor, i, 5 - j));
            }
            else
            {
               pit = new Gpit("", new PitIcon(3, stoneColor, i, j));
            }

            if (i == 0)
            {
               pit.setColumn(5 - j);
            }
            else
            {
               pit.setColumn(j);
            }

            if (i == 1)
            {
               pit.setPlayer("Player1");
            }
            else
            {
               pit.setPlayer("Player2");
            }

            final JButton b = new JButton();

            // decoration
            b.setBorderPainted(false);
            b.setBackground(color);

            final PitIcon img = pit.getStoneIcon();

            b.setIcon(img);
            centerPanel.add(b);
            b.addActionListener(new ActionListener()
            {
               @Override
               public void actionPerformed(ActionEvent arg0)
               {
                  String n = pit.getPlayer();
                  int nInt;

                  if (n.equalsIgnoreCase("Player1"))
                  {
                     nInt = 0;
                  }
                  else
                  {
                     nInt = 1;
                  }

                  // if correct user clicked
                  if (model.input(nInt, pit.getColumn()))
                  {
                     Component manc1 = rightPanel.getComponent(0);
                     final MancalaIcon imgMac1 = new MancalaIcon(
                             model.board[0][6].getStones(), stoneColor, mancalaA);
                     ((JButton) manc1).setIcon(imgMac1);


                     Component manc2 = leftPanel.getComponent(0);
                     final MancalaIcon imgMac2 = new MancalaIcon(
                             model.board[1][6].getStones(), stoneColor, mancalaB);
                     ((JButton) manc2).setIcon(imgMac2);


                     // get the components back
                     Component[] buttons = centerPanel.getComponents();

                     int x = 0;// counter
                     for (int i = 1; i >= 0; i--)
                     {
                        for (int j = 0; j < 6; j++)
                        {
                           message.setText("");
                           final Gpit pit;
                           // if first row
                           if (i == 0)
                           {
                              pit = new Gpit("", new PitIcon(
                                      model.board[i][j].getStones(), stoneColor, i - 1, j));
                           }
                           else
                           {
                              pit = new Gpit("", new PitIcon(
                                      model.board[i][5 - j].getStones(), stoneColor, i - 1, 5 - j));
                           }
                           // if second row
                           if (i == 0)
                           {
                              pit.setColumn(j);
                           }
                           else
                           {
                              pit.setColumn(5 - j);
                           }

                           if (i == 1)
                           {
                              pit.setPlayer("Player1");
                           }
                           else
                           {
                              pit.setPlayer("Player2");
                           }

                           final PitIcon img = pit.getStoneIcon();
                           ((JButton) buttons[x]).setIcon(img);
                           x++;
                        }
                     }
                     message.setText("IT IS PLAYER "
                             + (model.expectedPlayer + 1) + " TURN!");
                     undo.setEnabled(true);
                     frame.validate();
                     frame.repaint();
                  }
                  else
                  {
                     // check if no stones left
                     if (model.isGameOver() == true)
                     {



                        int p2 = model.board[0][6].getStones();
                        int p1 = model.board[1][6].getStones();

                        if (p2 == p1)
                        {
                           message.setText("GAME OVER :    TIE GAME!");
                           message.setForeground(Color.GREEN);

                        }
                        else if (p2 > p1)
                        {
                           message.setText("PLAYER 2 WINS!  SCORE : Player1  ("
                                   + p1
                                   + ")"
                                   + "    Player 2  ("
                                   + p2
                                   + ")");
                           message.setForeground(Color.GREEN);
                           return;
                        }
                        else
                        {
                           message.setText("PLAYER 1 WINS!  SCORE : Player2  ("
                                   + p2
                                   + ")"
                                   + "    Player 1  ("
                                   + p1
                                   + ")");
                           message.setForeground(Color.GREEN);
                           return;
                        }

                        return;
                     }
                     frame.validate();
                     frame.repaint();
                  }

               }// end actionPer...
            });// end AddAction...

         }// end inner for
      }// end outer for
      // end making button
   }// end startGame

   /**
    * Repaints board when called
    *
    * @param e event variable
    */
   @Override
   public void stateChanged(ChangeEvent e)
   {
      this.repaint();
   }
}// end View
