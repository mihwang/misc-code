
import java.util.ArrayList;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * Model class. Creates the functions of the game board.
 *
 * @author: Amru Eliwat, Matthew Hwang, Chau Ngo
 * @class: CS 151
 * @section 1
 * @Team: Area 151
 * @date: May 4th, 2013
 */
public class Model implements Cloneable
{

   public Pit[][] board;//new board with pits
   boolean extraTurn = false;//checks for extra turn
   private Pit[][] undo;//backup board 
   private boolean undoExtraTurn;//players extra turn
   boolean saved = true;//checks if the players saved or not, so that the player cannot undo twice
   int expectedPlayer;//expected players turn
   boolean once = true;//checks who went first
   private ArrayList<ChangeListener> listener;

   /**
    * Constructor. Creates pits using an ArrayList.
    *
    * @param stones number of stones for each pit
    */
   public Model(int stones)
   {
      board = new Pit[2][7];
      listener = new ArrayList<ChangeListener>();

      //put stones in each Pit
      for (int i = 0; i < 2; i++)
      {
         for (int j = 0; j < 7; j++)
         {
            board[i][j] = new Pit(stones);
         }
      }

      board[0][6] = new Pit(0); //left mancala initially no stones
      board[1][6] = new Pit(0); //right mancala initially no stones
   }

   /**
    * creates and adds a ChangeListener
    *
    * @param cl ChangeListener
    */
   public void addChangeListener(ChangeListener cl)
   {
      listener.add(cl);
   }

   /**
    * Automatically clears and credits stones if one player clears their row
    *
    * @param x 0 for first player. 1 for second player
    * @param y column number
    * @return true is board is solved, false if not
    */
   public boolean solve(int x, int y)//return true for end case
   {
      int orginalX = x;	//the players side
      int storedStones = board[x][y].getStones();//gets stone number
      board[x][y].setStones(0);//erase stones from selected pit

      while (storedStones != 0)
      {
         for (int i = y + 1; i < 7; i++)//loops through pits
         {
            if (!(orginalX != x && i == 6))
            {
               board[x][i].addStone();//add stone in pit
               storedStones--;
            }
            if (storedStones == 0)//last turn
            {
               //Determines which side still has stones and clears them
               if (orginalX == x && board[x][i].getStones() == 1 && i != 6)
               {
                  if (x == 0)//determines which player it is
                  {
                     if (board[1][5 - i].getStones() > 0)
                     {//if the pit across from capture case is empty
                        board[x][6].setStones(board[x][6].getStones() + board[1][5 - i].getStones() + 1);//take all opp0nents stones and place in your mancala
                        board[1][5 - i].setStones(0); //opposite pit to zero
                        board[x][i].setStones(0);
                     } //set your pit to zero
                  }
                  else
                  {
                     if (board[0][5 - i].getStones() > 0)
                     {//if the pit across from capture case is empty
                        board[x][6].setStones(board[x][6].getStones() + board[0][5 - i].getStones() + 1);//take all opponents stones and place in your mancala
                        board[0][5 - i].setStones(0); //opposite pit to zero
                        board[x][i].setStones(0);
                     } //set your pit to zero
                  }
               }
               //if your last stones is in your mancala you get an extra turn
               if (orginalX == x && i == 6)
               {
                  extraTurn = true;
               }

               int count0 = 0;//counts how many p1 pits are empty
               for (int z = 0; z < 6; z++)
               {
                  if (board[0][z].getStones() == 0)
                  {
                     count0++;
                  }
               }
               if (count0 == 6)
               {
                  for (int z = 0; z < 6; z++)//if all pits empty
                  {
                     board[1][6].setStones(board[1][6].getStones() + (board[1][z].getStones()));
                     board[1][z].setStones(0);//clears the pit
                  }
                  return true;
               }

               int count1 = 0;//counts how many p2 pits are empty
               for (int z = 0; z < 6; z++)
               {
                  if (board[1][z].getStones() == 0)
                  {
                     count1++;
                  }
               }
               if (count1 == 6) //if all pits are empty
               {
                  for (int z = 0; z < 6; z++)
                  {
                     board[0][6].setStones(board[0][6].getStones() + board[0][z].getStones());//add all stones from pits to mancala
                     board[0][z].setStones(0);//clears the pit
                  }
                  return true;
               }
               return false;
            }
         }
         if (x == 0)
         {
            x++;
         }
         else //at the end of the pits switch side
         {
            x--;
         }
         y = -1;
      }
      return false;
   }

   /**
    * Notifies if any data gets updated
    */
   private void notifyListeners()
   {

      // Notify all observers of the change to the Pits
      ChangeEvent event = new ChangeEvent(this);

      for (ChangeListener lis : listener)
      {
         lis.stateChanged(event);
      }
   }

   /**
    * Saves for undo function
    */
   public void save()
   {
      undo = new Pit[2][7];
      for (int i = 0; i < 2; i++)
      {
         for (int j = 0; j < 7; j++)
         {
            undo[i][j] = new Pit(board[i][j].getStones());
         }
      }
      undoExtraTurn = extraTurn;
      saved = true;
   }

   /**
    * Copies from save for undo function
    */
   public void load()
   {
      board = new Pit[2][7];

      for (int i = 0; i < 2; i++)
      {
         for (int j = 0; j < 7; j++)
         {
            board[i][j] = new Pit(undo[i][j].getStones());
         }
      }
      if (saved)
      {
         if (expectedPlayer == 0)
         {
            expectedPlayer++;
         }
         else
         {
            expectedPlayer--;
         }
         saved = false;
      }
   }

   /**
    * Helps Solve the game. Shows final score.
    *
    * @param i1 player
    * @param i2 player column
    * @return
    */
   boolean input(int i1, int i2)
   {
      if (once)
      {
         expectedPlayer = i1;
         once = false;
      }

      if (i1 == 2)
      {
         load();
         extraTurn = undoExtraTurn;
      }
      if (expectedPlayer == i1 && (board[i1][i2].getStones() != 0))
      {
         save();
         if (solve(i1, i2))
         {
            System.out.println("Player0: " + board[0][6].getStones());
            System.out.println("Player1: " + board[1][6].getStones());
         }
         if (!extraTurn)
         {
            if (expectedPlayer == 0)
            {
               expectedPlayer++;
            }
            else
            {
               expectedPlayer--;
            }
         }
         else
         {
            extraTurn = false;
         }
         return true;
      }
      else
      {
         return false;
      }
   }

   /**
    * Checks if game is over
    *
    * @return true is over, false if not
    */
   public boolean isGameOver()
   {

      boolean empty;
      //check player two  pits
      if (board[0][0].getStones() == 0 && board[0][1].getStones() == 0 && board[0][2].getStones() == 0
              && board[0][3].getStones() == 0 && board[0][4].getStones() == 0 && board[0][5].getStones() == 0)
      {
         empty = true;
         return empty;
      }
      //check player one pits
      else if (board[1][0].getStones() == 0 && board[1][1].getStones() == 0 && board[1][2].getStones() == 0
              && board[1][3].getStones() == 0 && board[1][4].getStones() == 0 && board[1][5].getStones() == 0)
      {
         empty = true;
         return empty;
      }
      return false; //both full
   }
}
