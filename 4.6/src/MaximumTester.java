
import java.util.ArrayList;
import java.util.Comparator;

/**
 * Finds and prints the longest string in an arraylist of strings
 *
 * @author Matthew Hwang CS 151 Homework 2
 */
public class MaximumTester
{

   /**
    * Finds longest string in an arraylist using compares
    *
    * @param a An arraylist of strings
    * @param c A comparator to compare string lengths
    * @return String of greatest length
    */
   public static String maximum(ArrayList<String> a, Comparator<String> c)
   {
      //if an empty array is passed
      if (a.isEmpty())
      {
         return "Empty Array";
      }
      //if arraylist has only one element
      if (a.size() == 1)
      {
         return a.get(0);
      }
      String longest = a.get(0);
      for (int i = 1; i < a.size(); i++)
      {
         if (c.compare(longest, a.get(i)) < 0)
         {
            longest = a.get(i);
         }
      }

      return longest;
   }

   /**
    * Comparator method for strings. Has a local inner class
    *
    * @param o1 first string
    * @param o2 second string
    * @return positive number of string 1 is bigger, 0 if same, and -1 if
    * smaller
    */
   public static class StringComparator implements Comparator<String>
   {

      @Override
      public int compare(String o1, String o2)
      {
         return o1.length() - o2.length();
      }
   }

   public static void main(String[] args)
   {
      Comparator<String> c = new StringComparator();
      ArrayList<String> test = new ArrayList<>();
      test.add("A");
      test.add("cat");
      test.add("is");
      test.add("dainty");
      test.add("insertword");
      String a = maximum(test, c);
      System.out.println("Longest is " + a);

      test.add("DogsAreBetter");
      a = maximum(test, c);
      System.out.println("New Longest string is " + a);
   }
}
