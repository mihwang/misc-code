
import java.util.ArrayList;

/**
 * Sorts arraylist of bank accounts
 * @author Matthew Hwang
 * cs 151
 * homework 2
 */
public class BankAccountTester
{
   /**
    * sorts bank accounts from lowest to highest
    * @param array arraylist of bank accounts
    * @return sorted array
    */
   public static ArrayList<BankAccount> sort(ArrayList<BankAccount> array)
   {
      
      ArrayList<BankAccount> temp = array;
      for(int i = 0; i < array.size(); i++)
      {
         BankAccount low = temp.get(i);
         int position = i;
         for(int j = i; j < array.size(); j++)
         {
            if(low.compareTo(array.get(j)) == 1)
            {
               low = array.get(j);
               position = j;
            }
         }
         //swaps elements at low index at index 0
         BankAccount t = array.get(i);
         temp.set(i, low);
         temp.set(position,t);
         
      }
      return temp;
   }
      public static void main(String[] args)
   {
      ArrayList<BankAccount> array = new ArrayList<>();
      BankAccount a1 = new BankAccount(50);
      array.add(a1);
      BankAccount a2 = new BankAccount(111);
      array.add(a2);
      BankAccount a3 = new BankAccount(832);
      array.add(a3);
      BankAccount a4 = new BankAccount(409);
      array.add(a4);
      BankAccount a5 = new BankAccount(12);
      array.add(a5);
      BankAccount a6 = new BankAccount(2834);
      array.add(a6);
      BankAccount a7 = new BankAccount(999);
      array.add(a7);
      BankAccount a8 = new BankAccount(1234);
      array.add(a8);
      BankAccount a9 = new BankAccount(67543);
      array.add(a9);
      BankAccount b1 = new BankAccount(1);
      array.add(b1);
      BankAccount b2 = new BankAccount(29);
      array.add(b2);
      
      //prints unsorted array
      System.out.println("Unsorted array is:");
      for(int i = 0; i < array.size(); i++)
      {
         BankAccount temp = array.get(i);
         System.out.print(temp.getBalance() + " ");
      }
      System.out.println();
      

      array = sort(array);
      //prints sorted array
      System.out.println("Sorted array is:");
            for(int i = 0; i < array.size(); i++)
      {
         BankAccount temp = array.get(i);
         System.out.print(temp.getBalance() + " ");
      }
      System.out.println();
   }
   
}
