
/**
 * Creates a bank account with a balance and compareto method
 *
 * @author Matthew Hwang cs 151 homework 2
 */
public class BankAccount implements Comparable
{

   private int balance;

   /**
    * initial constructor
    *
    * @param balance account balance
    */
   public BankAccount(int balance)
   {
      this.balance = balance;
   }

   /**
    * gets balance
    *
    * @return account balance
    */
   public int getBalance()
   {
      return balance;
   }

   /**
    * Compares balances of two BankAccounts and 0 is the same.
    *
    * @param o Bank account being compared to
    * @return -1 is first balance is smaller, 1 is bigger, and 0 is the same.
    */
   @Override
   public int compareTo(Object o)
   {
      BankAccount object = (BankAccount) o;
      if (balance < object.balance)
      {
         return -1;
      }
      if (balance > object.balance)
      {
         return 1;
      }
      return 0;
   }
}
