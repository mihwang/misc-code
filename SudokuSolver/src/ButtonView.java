
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * Creates buttons for each grid cell that do not do anything when pressed, but
 * button content changes while puzzle is being solved
 *
 * @author Matthew Hwang
 */
public class ButtonView extends JPanel implements ChangeListener
{

   public static final int GRID_MAX = 9;//max columns/rows
   private Model m = new Model();
   private JButton cell[][]; //new button

   /**
    * Constructor. Adds buttons to the sudoku grid.
    *
    * @param m the sudoku grid in 2d array form
    */
   public ButtonView(Model m)
   {
      this.m = m;
      setLayout(new GridLayout(GRID_MAX, GRID_MAX));
      cell = new JButton[GRID_MAX][GRID_MAX];
      for (int i = 0; i < GRID_MAX; i++)
      {
         for (int j = 0; j < GRID_MAX; j++)
         {
            cell[i][j] = new JButton();
            add(cell[i][j]);
         }
      }
   }

   /**
    * Event handler for button changes
    *
    * @param changeevent event
    */
   @Override
   public void stateChanged(ChangeEvent changeevent)
   {
      String s = m.toString();
      updateView(s);
   }

   /**
    * Updates the grid numbers
    *
    * @param s current number
    */
   public void updateView(String s)
   {
      int count = 0;
      for (int i = 0; i < GRID_MAX; i++)
      {
         for (int j = 0; j < GRID_MAX; j++)
         {
            cell[i][j].setText(s.substring(count, count + 1));
            count++;
         }
      }
   }
}
