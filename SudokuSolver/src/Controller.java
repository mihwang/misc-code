
/**
 * Given controller code for sudoku solver.
 * Code from homework instructions.
 *
 * @author Matthew Hwang
 */
public class Controller implements Runnable
{

   private Model model;

   /**
    * Class Constructor.
    * @param m model class housing a grid
    */
   public Controller(Model m)
   {
      model = m;
   }

   /**
    * Starts the controller
    */
   public void run()
   {
      {
         try
         {
            Thread.sleep(1000); // delay to see the initial position 
            model.solve(0, 0); // to solve the puzzle in the top left corner
         }
         catch (Exception e)
         {
         }
      }
   }
}