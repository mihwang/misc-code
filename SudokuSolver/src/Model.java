
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * Creates the structure model of a sudoku board. Has a hardcoded sudkoku
 * puzzle. Sudoku Puzzle is taken from this website:
 * http://www.websudoku.com/?level=3&set_id=5076406264
 *
 * Takes about an hour to solve the hard level puzzle
 *
 * @author Matthew Hwang
 * @CS 151, section 1
 * @date 4-13-2013
 * @homework 4
 */
public class Model
{

   public static final int GRID_MAX = 9;//max columns/rows
   public static final int SUBGRID_MAX = 3;//size of subgrids
   private int[][] grid = new int[GRID_MAX][GRID_MAX];
   ArrayList listeners = new ArrayList();//holds listeners for each grid cell

   /**
    * Constructor. Creates the sudoku board and fills in initial values
    */
   public Model()
   {
      //top left 3x3
      grid[0][0] = 0;
      grid[0][1] = 0;
      grid[0][2] = 0;
      grid[1][0] = 5;
      grid[1][1] = 9;
      grid[1][2] = 6;
      grid[2][0] = 0;
      grid[2][1] = 7;
      grid[2][2] = 0;
      //top middle 3x3
      grid[0][3] = 0;
      grid[0][4] = 8;
      grid[0][5] = 5;
      grid[1][3] = 0;
      grid[1][4] = 0;
      grid[1][5] = 0;
      grid[2][3] = 0;
      grid[2][4] = 0;
      grid[2][5] = 0;
      //top right 3x3
      grid[0][6] = 9;
      grid[0][7] = 0;
      grid[0][8] = 0;
      grid[1][6] = 0;
      grid[1][7] = 0;
      grid[1][8] = 0;
      grid[2][6] = 0;
      grid[2][7] = 5;
      grid[2][8] = 1;
      //middle left 3x3
      grid[3][0] = 0;
      grid[3][1] = 0;
      grid[3][2] = 0;
      grid[4][0] = 2;
      grid[4][1] = 6;
      grid[4][2] = 0;
      grid[5][0] = 0;
      grid[5][1] = 0;
      grid[5][2] = 0;
      //center 3x3
      grid[3][3] = 0;
      grid[3][4] = 5;
      grid[3][5] = 1;
      grid[4][3] = 0;
      grid[4][4] = 7;
      grid[4][5] = 0;
      grid[5][3] = 2;
      grid[5][4] = 6;
      grid[5][5] = 0;
      //middle right 3x3
      grid[3][6] = 0;
      grid[3][7] = 0;
      grid[3][8] = 0;
      grid[4][6] = 0;
      grid[4][7] = 8;
      grid[4][8] = 3;
      grid[5][6] = 0;
      grid[5][7] = 0;
      grid[5][8] = 0;
      //bottom left 3x3
      grid[6][0] = 6;
      grid[6][1] = 8;
      grid[6][2] = 0;
      grid[7][0] = 0;
      grid[7][1] = 0;
      grid[7][2] = 0;
      grid[8][0] = 0;
      grid[8][1] = 0;
      grid[8][2] = 7;
      //bottom middle 3x3
      grid[6][3] = 0;
      grid[6][4] = 0;
      grid[6][5] = 0;
      grid[7][3] = 0;
      grid[7][4] = 0;
      grid[7][5] = 0;
      grid[8][3] = 9;
      grid[8][4] = 2;
      grid[8][5] = 0;
      //bottom right 3x3
      grid[6][6] = 0;
      grid[6][7] = 2;
      grid[6][8] = 0;
      grid[7][6] = 1;
      grid[7][7] = 4;
      grid[7][8] = 7;
      grid[8][6] = 0;
      grid[8][7] = 0;
      grid[8][8] = 0;
   }

   /**
    * Checks to see if value in row exists
    *
    * @param x row number
    * @param y value to be checked
    * @return true if value is not on grid, false if yes
    */
   public boolean Row(int x, int y)
   {
      for (int i = 0; i < GRID_MAX; i++)
      {
         if (grid[x][i] == y)
         {
            return false;
         }
      }
      return true;
   }

   /**
    * checks to see if value in column exists
    *
    * @param x column number
    * @param y value to be checked
    * @return true if value does not exist, false if yes
    */
   public boolean Column(int x, int y)
   {
      for (int i = 0; i < GRID_MAX; i++)
      {
         if (grid[i][x] == y)
         {
            return false;
         }
      }
      return true;
   }

   /**
    * Checks the 3x3 grids
    *
    * @param x row
    * @param y column
    * @param z value to check
    * @return false if value exists, true if not
    */
   public boolean subGrid(int x, int y, int z)
   {
      x = (x / SUBGRID_MAX) * SUBGRID_MAX;
      y = (y / SUBGRID_MAX) * SUBGRID_MAX;
      for (int i = 0; i < SUBGRID_MAX; i++)
      {
         for (int j = 0; j < SUBGRID_MAX; j++)
         {
            if (grid[x + i][y + j] == z)
            {
               return false;
            }
         }
      }
      return true;
   }

   /**
    * Recursive solve method. Tries to solve the grid
    *
    * @param x row
    * @param y column
    * @throws Exception to stop recursion
    */
   public void solve(int x, int y) throws Exception
   {
      //Stops recursion
      if (x >= GRID_MAX)
      {
         throw new Exception("Puzzle Solved!");
      }
      if (grid[x][y] != 0)
      {
         next(x, y);
      }
      else
      {
         for (int i = 1; i < GRID_MAX + 1; i++)
         {
            if (!subGrid(x, y, i) || !Column(x, i) || !Row(y, i))
            {
               continue;
            }

            grid[x][y] = i;
            ChangeListener c;
            for (Iterator it = listeners.iterator(); it.hasNext(); c.stateChanged(new ChangeEvent(this)))
            {
               c = (ChangeListener) it.next();
            }

            Thread.sleep(1000);
            next(x, y);
         }

         grid[x][y] = 0;
         ChangeListener c2;
         for (Iterator it2 = listeners.iterator(); it2.hasNext(); c2.stateChanged(new ChangeEvent(this)))
         {
            c2 = (ChangeListener) it2.next();
         }
      }
   }

   /**
    * Recursive helper method for solve
    *
    * @param x row number
    * @param y column number
    * @throws Exception check to see if not past grid size
    */
   public void next(int x, int y) throws Exception
   {
      if (y < GRID_MAX - 1)
      {
         solve(x, y + 1);
      }
      else
      {
         solve(x + 1, 0);
      }
   }

   /**
    * Adds a listener to a button, which is a grid cell
    *
    * @param changelistener button listener
    */
   public void addButtonEvent(ChangeListener changelistener)
   {
      listeners.add(changelistener);
   }

   /**
    * Basic toString method
    *
    * @return string converted from int
    */
   @Override
   public String toString()
   {
      String s = "";
      for (int i = 0; i < GRID_MAX; i++)
      {
         for (int j = 0; j < GRID_MAX; j++)
         {
            s += Integer.toString(grid[i][j]);
         }
      }
      return s;
   }
}
