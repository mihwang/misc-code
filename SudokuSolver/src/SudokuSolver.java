
import java.awt.FlowLayout;
import javax.swing.JFrame;

/**
 * Creates and automatically solves a sudoku puzzle.
 * Some code taken from homework instructions and lecture notes
 * @author Matthew Hwang
 * @date 4-13-2013
 * @cs151, Section 1
 * @Homework 4
 */
public class SudokuSolver
{
   
   public static void main(String[] args)
   {

        Model model = new Model();
        JFrame frame = new JFrame();
        frame.setLayout(new FlowLayout());
        frame.setSize(400, 400);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        ButtonView button = new ButtonView(model);
        model.addButtonEvent(button);
        frame.add(button);

        Runnable r = new Controller(model); 
        Thread t = new Thread(r);
        t.start();
   }
}
