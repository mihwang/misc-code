
import java.util.ArrayList;
import java.util.Collections;

/**
 * CountryTester that tests country class. Initial test taken from text book
 * class cs151 homework 2
 *
 * @author Matthew Hwang
 */
public class CountryTester
{

   public static void main(String[] args)
   {
      ArrayList<Country> countries = new ArrayList<>();
      countries.add(new Country("Uruguay", 176220));
      countries.add(new Country("Thailand", 514000));
      countries.add(new Country("Belgium", 30510));

      //sorts countries by name in inreasing order
      Collections.sort(countries, Country.createComparatorByName(true));
      for (Country c : countries)
      {
         System.out.println(c.getName());
      }
      //sorts countries by area in increasing order
      Collections.sort(countries, Country.createComparatorByArea(true));
      for (Country c : countries)
      {
         System.out.println(c.getArea());
      }
   }
}
