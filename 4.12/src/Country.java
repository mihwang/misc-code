
import java.util.Comparator;

/**
 * Country class from TextBook that creates a country with an area Homework 2
 * Class cs151
 *
 * @author Matthew Hwang
 */
public class Country
{

   private String name;// name of country
   private double area;//area of country

   /**
    * Construct initial country
    *
    * @param aName country name
    * @param anArea country area
    */
   public Country(String aName, double anArea)
   {
      name = aName;
      area = anArea;
   }

   /**
    * gets country name
    *
    * @return country name
    */
   public String getName()
   {
      return name;
   }

   /**
    * gets country area
    *
    * @return country area
    */
   public double getArea()
   {
      return area;
   }

   /**
    * Method that uses an anonymous class to compare
    *
    * @param increasing can be true or false
    * @return anonymous class that compares two objects in increasing or
    * decreasing order
    */
   public static Comparator<Country> createComparatorByName(final boolean increasing)
   {
      return new Comparator<Country>()
      {
         @Override
         public int compare(Country country1, Country country2)
         {
            return country1.getName().compareTo(country2.getName());
         }
      };
   }

   /**
    * Method that uses an anonymous class to compare
    *
    * @param increasing can be true or false
    * @return anonymous class that compares two objects in increasing or
    * decreasing order
    */
   public static Comparator<Country> createComparatorByArea(final boolean increasing)
   {
      return new Comparator<Country>()
      {
         @Override
         public int compare(Country country1, Country country2)
         {
            Double a = country1.getArea();
            Double b = country2.getArea();
            return a.compareTo(b);
         }
      };
   }
}