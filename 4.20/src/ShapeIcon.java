
import java.awt.*;
import java.util.*;
import javax.swing.*;

/**
 * ShapeIcon class from textbook. Modified for homework problem.
 * @author Matthew Hwang
 */
public class ShapeIcon
{
   private int width;
   private int height;
   private MoveableShape shape;
   
   public ShapeIcon(MoveableShape shape, int width, int height)
   {
      this.shape = shape;
      this.width = width;
      this.height = height;
   }
   
   public int getIconWidth()
   {
      return width;
   }
   
   public int getIconeHeight()
   {
      return height;
   }
   
   public void paintIcon(Component c, Graphics g, int x, int y)
   {
      Graphics2D g2 = (Graphics2D) g;
      shape.draw(g2);
   }
   
   
}
