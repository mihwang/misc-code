
import java.awt.*;
/**
 * shape interface from textbook
 * @author Matthew Hwang
 * cs 151
 * homework 2
 */
public interface MoveableShape
{
   /**
    * draws the shape
    * @param g2 graphic context
    */
   void draw(Graphics2D g2);
   
   /**
    * moves the shape
    * @param dx amount moved in x axis
    * @param dy amount moved in y axis
    */
   
   
   void translate(int dx, int dy);
   
   //void draw(Graphics2D g2, int x, int y);
}
