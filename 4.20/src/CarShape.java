
import java.awt.*;
import java.awt.geom.*;
import java.util.*;

/**
 * Creates a car shape. Code is from textbook
 *
 * @author Matthew Hwang
 * 
 */
public class CarShape implements MoveableShape
{

   private int x;
   private int y;
   private int width;
   private int framewidth;

   /**
    * Constructor for car item
    * @param x initial x coordinate
    * @param y initial y coordinate
    * @param width width of rectangle
    */
   public CarShape(int x, int y, int width, int framewidth)
   {
      this.x = x;
      this.y = y;
      this.width = width;
      this.framewidth = framewidth;
   }

   public void translate(int dx, int dy)
   {
      x  = (x + dx) % framewidth;
      y += dy;
   }
   
   public void draw(Graphics2D g2)
   {
      draw(g2,x,y);
      if(x + width > framewidth)
      {
         draw(g2,x-framewidth,y);
      }
   }

   private void draw(Graphics2D g2,int x, int y)
   {
      Rectangle2D.Double body = new Rectangle2D.Double(x, y + width / 6,
              width - 1, width / 6);
      Ellipse2D.Double frontTire = new Ellipse2D.Double(x + width / 6,
              y + width / 3, width / 6, width / 6);
      Ellipse2D.Double rearTire = new Ellipse2D.Double(x + width * 2 / 3,
              y + width / 3, width / 6, width / 6);

      //Bottom of front windshield

      Point2D.Double r1 = new Point2D.Double(x + width / 6, y + width / 6);
      //the front roof
      Point2D.Double r2 = new Point2D.Double(x + width / 3, y);
      //the rear roof
      Point2D.Double r3 = new Point2D.Double(x + width * 2 / 3, y);
      //bottom of rear windshield
      Point2D.Double r4 = new Point2D.Double(x + width * 5 / 6, y + width / 6);
      Line2D.Double frontWindshield = new Line2D.Double(r1, r2);
      Line2D.Double roofTop = new Line2D.Double(r2, r3);
      Line2D.Double rearWindshield = new Line2D.Double(r3, r4);

      g2.draw(body);
      g2.draw(frontTire);
      g2.draw(rearTire);
      g2.draw(frontWindshield);
      g2.draw(roofTop);
      g2.draw(rearWindshield);

   }

}
