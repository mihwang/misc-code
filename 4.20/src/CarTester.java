
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
/**
 * Draws several moving cars. Some code from textbook and lecture notes
 * @author Matthew Hwang
 * cs151
 * homework 2
 */
public class CarTester extends JPanel
{
   private static final int ICON_WIDTH = 400;
   private static final int ICON_HEIGHT = 100;
   private static final int CAR_WIDTH = 100;
   
   
   
   MoveableShape s;

      public CarTester(MoveableShape m)
      {
         s = m;
      }

      public void paintComponent(Graphics g)
      {
         super.paintComponent(g);
         s.draw((Graphics2D) g);
      }
   

   public static void main(String[] args)
   {
      JFrame frame = new JFrame();
      int DEFAULT_WIDTH = 400;
      int DEFAULT_HEIGHT = 400;
      frame.setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
      int CAR_WIDTH = 100;
      final MoveableShape shape = new CarShape(0, 0, CAR_WIDTH,DEFAULT_WIDTH);
      final JPanel panel = new CarTester(shape);
      frame.add(panel);
      final int DELAY = 100;
// Milliseconds between timer ticks
      Timer t = new Timer(DELAY, new ActionListener()
      {
         public void actionPerformed(ActionEvent event)
         {
            shape.translate(1, 0);
            panel.repaint();
            
         }
      });
      t.start();
      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      frame.setVisible(true);
}
}
