
import java.awt.Component;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * Program that creates a slider which changes the size of a shape
 * @author Matthew Hwang
 * Problem 5.4
 */
public class CarSlider extends JPanel implements ChangeListener
{

   private CarShape c;
   private JPanel p;
   private int carWidth = 0;//original size of shape
   private int ADD = 10;// Magnitude of each slide tick

   /**
    * Slider constructor that also adds a shape
    * @param car the shape added
    */
   public CarSlider(CarShape car)
   {
      c = car;
      carWidth = c.getWidth();
      //creates the slider
      setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
      JLabel slider = new JLabel("Car Size", JLabel.CENTER);
      slider.setAlignmentX(Component.CENTER_ALIGNMENT);
      JSlider changeSize = new JSlider(JSlider.HORIZONTAL, 0, 5, 0);
      changeSize.addChangeListener(this);
      changeSize.setMajorTickSpacing(1);
      changeSize.setMinorTickSpacing(1);
      changeSize.setPaintTicks(true);
      changeSize.setPaintLabels(true);

      p = new CarTester(c);
      p.setAlignmentX(Component.CENTER_ALIGNMENT);
      p.setBorder(BorderFactory.createCompoundBorder(
              BorderFactory.createLoweredBevelBorder(),
              BorderFactory.createEmptyBorder(100, 100, 100, 100)));

      add(slider);
      add(changeSize);
      add(p);
   }

   @Override
   /**
    * ChangeListener that responds to when the slider is moved. 
    * Resizes the car shape.
    */
   public void stateChanged(ChangeEvent e)
   {
      JSlider slider = (JSlider) e.getSource();
      int m = (int) slider.getValue() * ADD;// Value that can fit the frame
      if (slider.getValueIsAdjusting())
      {
         c.setWidth(carWidth + m);
         p.repaint();
      }
   }
}
