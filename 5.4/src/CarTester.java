
import java.awt.*;
import javax.swing.*;


/**
 * Main method for slider program. Some code taken from homework 2
 *
 * @author Matthew Hwang
 * Problem 5.4
 * 
 */
public class CarTester extends JPanel
{

   private static final int CAR_WIDTH = 100;
   CarShape s;

   /**
    * Constructor that takes a shape
    * @param m the shape
    */
   public CarTester(CarShape m)
   {
      s = m;
   }

   /**
    * Paint method for the shape
    * @param g what will be drawn
    */
   public void paintComponent(Graphics g)
   {
      super.paintComponent(g);
      s.draw((Graphics2D) g);
   }

   public static void main(String[] args)
   {
      JFrame frame = new JFrame();
      int DEFAULT_WIDTH = 400;
      int DEFAULT_HEIGHT = 400;
      frame.setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
      final CarShape shape = new CarShape(0, 0, CAR_WIDTH, DEFAULT_WIDTH);
      CarSlider a = new CarSlider(shape);
      frame.add(a);
      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      frame.setVisible(true);
   }
}