
/**
 *
 * @author Matthew
 */
import java.awt.*;
import javax.swing.*;

public class ButtonTester extends JFrame{
    
    public static void main(String[] args)
    {
        JFrame frame = new JFrame();
        //frame.setSize(500, 500);
        JLabel label = new JLabel();
        
        JButton redButton = new JButton("Red");
        //redButton.setSize(100, 100);
        JButton blueButton = new JButton("Blue");
        //blueButton.setSize(100, 100);
        JButton greenButton = new JButton("Green");
        //greenButton.setSize(100, 100);
        
        frame.setLayout(new FlowLayout());
        frame.add(label);
        frame.add(redButton);
        frame.add(blueButton);
        frame.add(greenButton);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
    
}
