/**
 *C151 HW 1
 *author: Matthew Hwang
 *date: 2/17/2013
 * 
 * Creates a calendar that acts as a scheduler
 */
package sjsu.cs151;

import java.util.Scanner;


public class Calendar {

   public static void main(String args[]) {

      Menu menu = new Menu();

      Scanner in = new Scanner(System.in);
      String input = "";
      //Recieves input from the console for next step
      while (!input.equalsIgnoreCase("q")) {
         System.out.println("Select one of the following options: ");
         System.out.println("[V]iew [C]reate [G]oTo [E]ventList [D]elete [Q]uit");

         input = in.nextLine();
         // if input is to create
         if (input.equalsIgnoreCase("c")) {
            System.out.println("Please enter an event title.");
            String title = in.nextLine();
            System.out.println("Please enter a date in the format mm/dd/yyyy.");
            String date = in.nextLine();
            System.out.println("Please enter a starting time in the format hh:mm.");
            String start = in.nextLine();
            System.out.println("Please enter an ending time in the format hh:mm or enter 'none'");
            String end = in.nextLine();
            Scanner in2 = new Scanner(date);
            in2.useDelimiter("[/\n]");
            int month = in2.nextInt();
            int day = in2.nextInt();
            int year = in2.nextInt();
            in2.close();
            menu.Create(day, month, year, title, start, end);

         }
         // if input is to delete
         if (input.equalsIgnoreCase("d")) {
            System.out.println("Select one of the options.");
            System.out.println("[S]elected or [A]ll");
            String selection = in.nextLine();
            if (selection.equalsIgnoreCase("s")) {
               System.out.println("Please enter a date for the desired event in the format mm/dd/yyyy.");
               String date = in.nextLine();
               Scanner in2 = new Scanner(date);
               in2.useDelimiter("[/\n]");
               int month = in2.nextInt();
               int day = in2.nextInt();
               int year = in2.nextInt();
               in2.close();
               menu.Delete(input, day, month, year);
            }
         }

         //if input is to goto
         if (input.equalsIgnoreCase("g")) {
            System.out.println("Please enter a date for the desired event in the format mm/dd/yyyy.");
            String date = in.nextLine();
            Scanner in2 = new Scanner(date);
            in2.useDelimiter("[/\n]");
            int month = in2.nextInt();
            int day = in2.nextInt();
            int year = in2.nextInt();
            in2.close();
            menu.Goto(day, month, year);
         }

         //if input is to view
         if (input.equalsIgnoreCase("v")) {
            System.out.println("[D]ay view or [M]view ?");
            input = in.nextLine();
            if (input.equalsIgnoreCase("d")) {
               menu.ViewDay("");
               while (!input.equalsIgnoreCase("m")) {
                  System.out.println("[P]revios or [N]ext or [M]ain menu ? ");
                  input = in.nextLine();
                  menu.ViewDay(input);
               }
            }
            if (input.equalsIgnoreCase("m")) {
               input = "";
               menu.ViewMonth("");
               while (!input.equalsIgnoreCase("m")) {
                  System.out.println("[P]revios or [N]ext or [M]ain menu ? ");
                  input = in.nextLine();
                  menu.ViewMonth(input);
               }
            }
         }


      }

   }
}
