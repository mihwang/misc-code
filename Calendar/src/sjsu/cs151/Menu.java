/**
 *C151 HW 1
 *author: Matthew Hwang
 *date: 2/17/2013
 * 
 * Activates methods based on input choices to act like a menu.
 */
package sjsu.cs151;

public class Menu {

   private EventList events = new EventList();
   private CreateCalendar cal = new CreateCalendar();

   /**
    * Lets user view events per day
    * @param selection n for next, and p for previous
    */
   public void ViewDay(String selection) {
      int day = cal.getDay();
      int month = cal.getMonth();
      int year = cal.getYear();

      if (selection.equalsIgnoreCase("n")) {
         day = day + 1;
         events.getEvent(day, month, year);
      } else if (selection.equalsIgnoreCase("p")) {
         day = day - 1;
         events.getEvent(day, month, year);
      } else {
         //current day
         events.getEvent(day, month, year);
      }


   }

   /**
    * Lets user view months
    * @param selection  n for next, and p for previous
    */
   public void ViewMonth(String selection) {
      int day = cal.getDay();
      int month = cal.getMonth();
      int year = cal.getYear();
      {
         if (selection.equalsIgnoreCase("n")) {
            month = month + 1;
            events.getEvent(day, month, year);
         } else if (selection.equalsIgnoreCase("p")) {
            month = month - 1;
            events.getEvent(day, month, year);
         } else {
            //current month
            cal.Month(day, month, year);
         }
      }
   }

   /**
    * Lets user delete an event
    * @param input s for single event, a for all events
    * @param day event day
    * @param month event month
    * @param year event year
    */
   public void Delete(String input, int day, int month, int year) {

      if (input.equalsIgnoreCase("s")) {
         events.deleteEvent(day, month, year);
      } else if (input.equalsIgnoreCase("a")) {
         events.deleteAll();
      }
   }

   /**
    * Allows user to create an event
    * @param day event day
    * @param month event month
    * @param year event year
    * @param title event title
    * @param start event start time
    * @param end event end time or none
    */
   public void Create(int day, int month, int year, String title, String start, String end) {
      events.CreateEvent(day, month, year, title, start, end);
   }

   /**
    * Allows user to go to a certain day
    * @param day selected day
    * @param month selected month
    * @param year selected year
    */
   public void Goto(int day, int month, int year) {
      cal.Month(day, month, year);
   }
}
