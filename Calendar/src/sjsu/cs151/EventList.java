/**
 *C151 HW 1
 *author: Matthew Hwang
 *date: 2/17/2013
 * 
 * manages, deletes, creates events
 */
package sjsu.cs151;

import java.util.ArrayList;
import java.util.Collection;
import java.util.TreeMap;
import java.io.*;

/**
 *
 * @author Matt
 */
public class EventList {

   private TreeMap<Integer, TreeMap<Integer, TreeMap<Integer, String>>> eventMap = new TreeMap<>();
   private TreeMap<Integer, TreeMap<Integer, String>> monthMap = new TreeMap<>();
   private TreeMap<Integer, String> dayMap = new TreeMap<>();
   public static final int DAYS = 31;
   private String[] Months = {
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December"};



   /**
    * Creates an event
    * @param day selected day
    * @param month selected month
    * @param year selected year
    * @param title title of event
    * @param start start time of event
    * @param end end of event or none
    */
   public void CreateEvent(int day, int month, int year, String title, String start, String end) {

      String date = Months[month - 1] + " " + day + ", " + year;
      String time;
      if (end.equalsIgnoreCase("none")) {
         time = start;
      } else {
         time = start + " - " + end;
      }
      dayMap.put(day, title + " " + date + " " + time);
      monthMap.put(month, dayMap);
      eventMap.put(year, monthMap);


   }

   /**
    * prints out event on selected time
    * @param day selected day
    * @param month selected month
    * @param year selected year
    */
   public void getEvent(int day, int month, int year) {
      if (eventMap.containsKey(year) && monthMap.containsKey(month) && dayMap.containsKey(day)) {
         System.out.println("The Event for the day is:");
         System.out.println(eventMap.get(year).get(month).get(day));
      } else {
         String date = month + "/" + day + "/" + year;
         System.out.println("No events for " + date + "!");
      }
   }

   /**
    * deletes a event
    * @param day selected day
    * @param month selected month
    * @param year selected year
    */
   public void deleteEvent(int day, int month, int year) {
      eventMap.get(year).get(month).remove(day);
      String date = month + "/" + day + "/" + year;
      System.out.println("Event on " + date + "has been deleted.");
   }

   /**
    * deletes all events
    */
   public void deleteAll() {
      eventMap.clear();
      System.out.println("Events have been cleared.");
   }

   /**
    * Prints out all events in order of time
    */
   public void viewAll() {

      int year = eventMap.firstKey();
      int printYear = year;
      int month = monthMap.firstKey();
      Collection<String> a = eventMap.get(year).get(month).values();
      ArrayList<String> t = new ArrayList<>(a);
      System.out.println(printYear);
      for (int i = 0; i < t.size(); i++) {
         if (!t.get(i).contains(year + "")) {
            printYear = eventMap.higherKey(printYear);
            System.out.println(printYear);
         }

         System.out.println(t.get(i));

      }
   }

   /**
    * prints out all events on reservation.txt
    */
   public void Reservation() {
      try {
         // Create file 
         FileWriter fstream = new FileWriter("reservation.txt");
         BufferedWriter out = new BufferedWriter(fstream);

         int year = eventMap.firstKey();
         int printYear = year;
         int month = monthMap.firstKey();
         Collection<String> a = eventMap.get(year).get(month).values();
         ArrayList<String> t = new ArrayList<>(a);
         out.write(printYear);
         for (int i = 0; i < t.size(); i++) {
            if (!t.get(i).contains(year + "")) {
               printYear = eventMap.higherKey(printYear);
               out.write(printYear);
            }

            out.write(t.get(i));

         }
      } catch (Exception e) {//Catch exception if any
         System.err.println("Error: " + e.getMessage());
      }

   }
}
