/**
 *C151 HW 1
 *author: Matthew Hwang
 *date: 2/17/2013
 * 
 * Creates a calendar in text form
 */
package sjsu.cs151;

import java.util.Calendar;

public class CreateCalendar {

   Calendar cal = Calendar.getInstance();
   private EventList event = new EventList();
   public static final int DAYS = 7; // Number of days in a week or columns
   public static final int DOUBLE_DIGIT = 10;
   private int currentDay;
   private int currentMonth;
   private int currentYear;
   private String[] Months = {
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December"};

   /**
    * Constructor that creates month based on current day
    */
   public CreateCalendar() {
      int day = cal.get(Calendar.DAY_OF_MONTH);
      int month = cal.get(Calendar.MONTH) + 1;
      int year = cal.get(Calendar.YEAR);
      currentDay = day;
      currentMonth = month;
      currentYear = year;
      Month(day, month, year);
   }

   /**
    * Creates month based on inputs
    * @param day selected day
    * @param month selected month
    * @param year selected year
    */
   public void Month(int day, int month, int year) {
      cal.clear();
      cal.set(year, month - 1, day);
      cal.set(Calendar.DAY_OF_MONTH, 1);
      currentDay = day;
      currentMonth = month;
      currentYear = year;
      System.out.println("   " + Months[month - 1] + " " + year);
      System.out.println("   Su   Mo   Tu   We   Th   Fr   Sa");
      int first = cal.get(Calendar.DAY_OF_WEEK);
      int dayCount = 1;
      int weekCount = 1;
      int daysInMonth = cal.getActualMaximum(Calendar.DAY_OF_MONTH);

      //Fills in the first week of the month
      for (int i = 0; i < first - 1; i++) {
         System.out.print("     ");
         weekCount++;
      }

      while (dayCount <= daysInMonth) {
         if (weekCount > DAYS) {
            weekCount = 1;
         }
         if (dayCount == day) {
            dayMarker(dayCount, weekCount);
         } else if (weekCount != DAYS && dayCount < DOUBLE_DIGIT) {
            System.out.print("    " + dayCount);
         } else if (weekCount == DAYS && dayCount < DOUBLE_DIGIT) {
            System.out.println("    " + dayCount);
         } else if (weekCount != DAYS && dayCount >= DOUBLE_DIGIT) {
            System.out.print("   " + dayCount);
         } else if (weekCount == DAYS && dayCount >= DOUBLE_DIGIT) {
            System.out.println("   " + dayCount);
         }
         weekCount++;
         dayCount++;
      }
      System.out.println("\n");
      event.getEvent(day, month, year);
   }

   /**
    * Helper class that highlights the current day
    * @param day current day
    * @param dayOfWeek current week
    */
   public void dayMarker(int day, int dayOfWeek) {
      if (dayOfWeek != DAYS && day < DOUBLE_DIGIT) {
         System.out.print("  [" + day + "]");
      } else if (dayOfWeek == DAYS && day < DOUBLE_DIGIT) {
         System.out.println("  [" + day + "]");
      } else if (dayOfWeek != DAYS && day >= DOUBLE_DIGIT) {
         System.out.print(" [" + day + "]");
      } else if (dayOfWeek == DAYS && day >= DOUBLE_DIGIT) {
         System.out.println(" [" + day + "]");
      }
   }

   /**
    * gets the current day
    * @return current day
    */
   public int getDay() {
      return currentDay;
   }

   /**
    * gets the current month
    * @return current month
    */
   public int getMonth() {
      return currentMonth;
   }

   /**
    * gets the current year
    * @return current year
    */
   public int getYear() {
      return currentYear;
   }
}
