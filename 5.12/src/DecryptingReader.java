
import java.io.IOException;
import java.io.Reader;

/**
 * Decrypts the caesar cipher by shifting letter back by 3
 * @author Matthew Hwang
 */
public class DecryptingReader extends Reader
{
   private Reader r;
   
   /**
    * Constructor
    * @param r reader
    */
   public DecryptingReader(Reader r)
   {
      this.r = r;
   }

   /**
    * reads and writes to a character array
    * @param cbuf a character array containing text
    * @param off the offset to start in the array
    * @param len the length to read in the array
    * @return 
    * @throws IOException 
    */
   @Override
   public int read(char[] cbuf, int off, int len) throws IOException
   {
      return 0;
   }

   @Override
   public void close() throws IOException
   {
      r.close();
   }
   
}
