
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Program that encrypts and decrypts text using a caesar cipher
 * 
 * @author Matthew Hwang
 * Problem 5.12
 */
public class WriterTester
{
   public static void main(String[] args) throws IOException
   {
      EncryptingWriter ew = new EncryptingWriter(new FileWriter("new.txt"));
      ew.write("ABCDE", 0, 5);//should return DEFGH
      PrintWriter w = new PrintWriter(ew, true);
      w.println();
   }
}
