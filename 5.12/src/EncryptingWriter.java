
import java.io.IOException;
import java.io.Writer;

/**
 * Encryption program that shifts a letter by 3
 * @author Matthew Hwang
 */
public class EncryptingWriter extends Writer
{
   private Writer w;
   
   /**
    * Constructor
    * @param w writer
    */
   public EncryptingWriter(Writer w)
   {
      this.w = w;
   }

   /**
    * reads and rewrites to character array
    * @param cbuf character array
    * @param off the offset where the reader starts
    * @param len length that reader reads
    * @throws IOException 
    */
   @Override
   public void write(char[] cbuf, int off, int len) throws IOException
   {
      char[] c = cbuf;
      for(int i = off; i < len; i++)
      {
         char n = c[i];
         if(Character.isLetter(n))
         {
            n = (char) ((int) n + 3);
            c[i] = n;
         }
         else
         {
            c[i] = n;
         }

      }
      w.write(c, off, len);
   }

   /**
    * Flushes the writer
    * @throws IOException 
    */
   @Override
   public void flush() throws IOException
   {
      w.flush();
   }

   /**
    * Closes the writer
    * @throws IOException 
    */
   @Override
   public void close() throws IOException
   {
      w.close();
   }
   
}
