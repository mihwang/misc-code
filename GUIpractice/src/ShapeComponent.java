
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Panel;
import java.awt.Point;
import java.util.ArrayList;
import javax.swing.JComponent;

/**
 * Draws shapes on a component
 * @author Matt
 */
public class ShapeComponent extends JComponent
{

   private ArrayList<CompositeShape> shapes;
   private Point mousePoint;
   private CompositeShape selected;
   private Panel panel;
   
   private int x;
   private int y;

   /**
    * COnstructor
    * @param p panel
    * @param s shape
    * @param x x coordinate
    * @param y y coordinate
    */
   public ShapeComponent(Panel p,CompositeShape s,int x, int y)
   {
      selected = s;
      this.x = x;
      this.y = y;
      panel = p;
      /**
      shapes = new ArrayList<CompositeShape>();
      p.addMouseListener(new MouseAdapter()
      {
         public void mousePressed(MouseEvent event)
         {
            mousePoint = event.getPoint();
            for (CompositeShape s : shapes)
            {
               //if (s.contains(mousePoint))
               {
               //   s.setSelected(!s.isSelected());
               }
            }
            repaint();
         }
      });
      */
   }

   /**
    * Basic paint method
    * @param g graphics painted
    */
   public void paintComponent(Graphics g)
   {
      super.paintComponent(g);
      Graphics2D g2 = (Graphics2D) g;
      //for (CompositeShape s : shapes)
      //{
      ShapeIcon icon = new ShapeIcon(selected,50,50);
         icon.paintIcon(panel, g, x, y);
        // if (s.isSelected())
        // {
        //    s.drawSelection(g2);
         //}
      //}
   }

   /**
    * Adds a shape to the scene.
    *
    * @param s the shape to add
    */
   //public void add(CompositeShape s)
   //{
   //   shapes.add(s);
   //   repaint();
   //}

}
