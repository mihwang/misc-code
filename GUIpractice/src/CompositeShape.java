
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;

/**
 * A shape that uses the CompositeShapeInt interface
 * @author Matthew Hwang
 */
class CompositeShape implements CompositeShapeInt
{

   private Shape s;


   @Override
   public void add(Shape aShape)
   {
      s = aShape;
   }

   @Override
   public Rectangle getBounds()
   {
      Rectangle r = new Rectangle();
      r.height = 1;
      r.width = 1;
      return r;
   }

   @Override
   public void draw(Graphics2D g)
   {
      g.draw(s);
   }
   
   public Shape getShape()
   {
      return s;
   }
}
