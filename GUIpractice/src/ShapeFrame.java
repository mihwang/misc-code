
import java.awt.BorderLayout;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;

/**
 * Acts like a Jframe. Adds a box to hold buttons and a panel.
 * @author Matthew Hwang
 */
class ShapeFrame extends JComponent
{

   private Box buttons = new Box(BoxLayout.LINE_AXIS);
   private JFrame frame;
   private Panel panel = new Panel();
   private CompositeShape selected;
   private ShapeIcon icon;
   private ShapeComponent comp;
   private int x;
   private int y;

   /**
    * Constructor. Creates a frame and adds buttons and a panel to it.
    */
   public ShapeFrame()
   {
      frame = new JFrame();
      frame.add(buttons, BorderLayout.NORTH);
      frame.add(panel);
   }

   /**
    * Adds a buttons
    * @param c the shape that will be shown on the button
    */
   public void addShape(CompositeShape c)
   {
      final CompositeShape shape = c;
      final ShapeIcon s = new ShapeIcon(c, 50, 50);
      icon = s;
      JButton b = new JButton(s);
      //comp.add(c);
      b.addActionListener(new ActionListener()
      {
         public void actionPerformed(ActionEvent event)
         {
            
            selected = shape;
         }
      });
      panel.addMouseListener(new MouseAdapter()
      {
         public void mousePressed(MouseEvent event)
         {
            x = event.getX();
            y = event.getY();
            addShapePanel(x,y);
         }
      });
      buttons.add(b);
   }

   /**
    * method used for drawing to panel
    * @param x x coordinate
    * @param y y coordinate
    */
   public void addShapePanel(int x, int y)
   {
      //ShapeIcon s = new ShapeIcon(selected, 50, 50);
      ShapeComponent comp = new ShapeComponent(panel,selected,x,y);
   }

   /**
    * shows the frame
    * @param b true or false
    */
   public void setVisible(boolean b)
   {
      if (b == true)
      {
         frame.setVisible(true);
      }
      else
      {
         frame.setVisible(false);
      }
   }

   /**
    * sets frame size
    * @param i height
    * @param i0 width
    */
   public void setSize(int i, int i0)
   {
      frame.setSize(i, i0);
   }

   /**
    * sets frame title
    * @param s title string
    */
   public void setTitle(String s)
   {
      frame.setTitle(s);
   }

   public void setDefaultCloseOperation(int EXIT_ON_CLOSE)
   {
      frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
   }
}
