
import java.awt.Graphics2D;

/**
 * Creates a curvy line using arcs
 *
 * @author Matthew Hwang
 */
class Swirl extends CompositeShape
{

   private int x;
   private int y;
   private int width;

   /**
    * Constructor
    * @param i x coordinate
    * @param i0 y coordinate
    * @param i1 width of each arc
    */
   public Swirl(int i, int i0, int i1)
   {
      x = i;
      y = i0;
      width = i1;
   }

   /**
    * sets the width
    * @param x new value
    */
   public void setWidth(int x)
   {
      width = x;
   }

   /**
    * gets the width
    *
    * @return current width
    */
   public int getWidth()
   {
      return width;
   }

   public void draw(Graphics2D g2)
   {
      draw(g2, x, y);
   }

   private void draw(Graphics2D g2, int x, int y)
   {

      g2.drawArc(x, y, width, width, 0, 180);
      g2.drawArc(x + width, y, width, width, 0, -180);
      g2.drawArc(x + width * 2, y, width, width, 0, 180);
      g2.drawArc(x + width * 3, y, width, width, 0, -180);

   }
}