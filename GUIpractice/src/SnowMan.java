
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;

/**
 * Creates snowman using two ellipses.
 *
 * @author Matthew Hwang
 */
class SnowMan extends CompositeShape
{

   private int x;
   private int y;
   private int width;

   /**
    * Constructor
    *
    * @param i x coordinate
    * @param i0 y coordinate
    * @param i1 width
    */
   public SnowMan(int i, int i0, int i1)
   {
      x = i;
      y = i0;
      width = i1;
   }

   /**
    * sets the width
    *
    * @param x new width value
    */
   public void setWidth(int x)
   {
      width = x;
   }

   /**
    * gets the width
    *
    * @return current width
    */
   public int getWidth()
   {
      return width;
   }

   public void draw(Graphics2D g2)
   {
      draw(g2, x, y);
   }

   private void draw(Graphics2D g2, int x, int y)
   {

      Ellipse2D.Double top = new Ellipse2D.Double(x + width / 2,
              y + width / 2, width, width);
      Ellipse2D.Double bottom = new Ellipse2D.Double(x + width / 2,
              y + width * 3 / 2, width, width);

      g2.draw(top);
      g2.draw(bottom);


   }
}
